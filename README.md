# NotesThing - a single page notes taking app

Simple notes taking app to scratch my own itch.

## How to use

1. Create a note by opening the file in chrome or just go to https://nilstastic.gitlab.io/notesthing/
2. Write a note
3. Press ctrl+s to save your note
4. To search for a note, use the search box top right
5. To create a new note, reload the page.

## Roadmap

1. ~~List all notes~~
2. Show creation date
3. ~~Delete note~~
